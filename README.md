# Interaction-Flashlight Feature

1. Get close to an object and press E to interact and possess that object where you can rotate and analize it. After analized press again E to end the interaction.

2. Press F to turn on/off the flashlight.
