// Fill out your copyright notice in the Description page of Project Settings.


#include "InteractableObject.h"
#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AInteractableObject::AInteractableObject()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	RootComponent = SceneComponent;

	ObjectMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ObjectMesh"));
	ObjectMesh->SetupAttachment(RootComponent);
}

// Called to bind functionality to input
void AInteractableObject::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	//mouse horizontal movement control y axis
	PlayerInputComponent->BindAxis("Vertical", this, &AInteractableObject::HorizontalRotation);
	//mouse vertical movement control x axis
	PlayerInputComponent->BindAxis("Horizontal", this, &AInteractableObject::VerticalRotation);

	PlayerInputComponent->BindAction("Interact", IE_Pressed, this, &AInteractableObject::UnPossessObject);
}

void AInteractableObject::HorizontalRotation(float value)
{
	if (!isInteract)
	{
		AddActorLocalRotation(FRotator(0, value, 0));
	}
}

void AInteractableObject::VerticalRotation(float value)
{
	if (!isInteract)
	{
		AddActorLocalRotation(FRotator(value, 0, 0));
	}
}

void AInteractableObject::UnPossessObject()
{
	if (isCurrentlyPossessed)
	{
		OurObjectController = UGameplayStatics::GetPlayerController(this, 0);

		if (!SavedObject)
		{
			SavedObject = GetController();
		}

		//unpossess player
		SavedObject->UnPossess();

		//possess interactable object
		SavedObject->Possess(MyPlayer);

		OurObjectController->SetViewTarget(MyCamera);

		OurObjectController->SetViewTargetWithBlend(MyPlayer, CameraBlendTime);

		//timer
		GetWorld()->GetTimerManager().SetTimer(MovementDelayHandle, this, &AInteractableObject::AllowCharacterMovement, MovementDelayTime, false);	
	}
}

void AInteractableObject::AllowCharacterMovement()
{
	MyPlayer->GetCharacterMovement()->SetMovementMode(EMovementMode::MOVE_Walking);

	isCurrentlyPossessed = false;
	MyPlayer->isInteract = false;

	GetWorldTimerManager().ClearTimer(MovementDelayHandle);
}





