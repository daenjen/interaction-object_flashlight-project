// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/Character.h"
#include <GameTitle/CharacterMovement.h>
#include "InteractableObject.generated.h"

UCLASS()
class GAMETITLE_API AInteractableObject : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AInteractableObject();

	/*UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Mesh)
		UBoxComponent* BoxCollider;*/

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "InteractableObject")
		UStaticMeshComponent* ObjectMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "InteractableObject")
		USceneComponent* SceneComponent;

	UPROPERTY(EditAnywhere, Category = "Possession")
		AActor* MyCamera;

	UPROPERTY(EditAnywhere, Category = "Possession")
		ACharacterMovement* MyPlayer;

	UPROPERTY(EditAnywhere, Category = "Possession")
		float CameraBlendTime = 2.f;

	UPROPERTY(EditAnywhere, Category = "Possession")
		float MovementDelayTime = 2.f;

public:
	//possession
	bool isCurrentlyPossessed;
	bool isInteract;

	void HorizontalRotation(float);
	void VerticalRotation(float);

	void UnPossessObject();
	void AllowCharacterMovement();

private:
	//object controller
	AController* SavedObject;
	APlayerController* OurObjectController;
	ACharacterMovement* Player;

	//timer
	FTimerHandle MovementDelayHandle;

public:
	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
};
